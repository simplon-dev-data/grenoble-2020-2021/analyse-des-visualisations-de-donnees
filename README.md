# Interprétabilité des visualisations de données

## Mise en application 1 (en individuel)

![Covid_India_2](Covid_India_2.jpg)

Dans cette partie, nous allons travailler sur une visualisation de données issues du compte [Twitter du gouvernement indien](https://twitter.com/COVIDNewsByMIB) sur le COVID.
Le graphique suivant montre le nombre de personnes qui ont et qui ont eu le Covid en Inde entre le 1er juin 2020 et le 10 mai 2021. 

![Covid_India](Covid_India.jpg)

Questions :

- Le choix de la visualisation de données vous semble-t-il pertinent ?

- Que cherche à montrer ce graphique ? Voici le tweet du compte officiel du gouvernement indien :

![Covid_India_3](Covid_India_3.jpg)

- A l'aide d'un outil de visualisation de données (Google Sheet, Metabase, Power BI, Matplotlib, Seaborn, Plotly...), créer un (ou des) graphique permettant de 
**bien interpréter** les données.

## Mise en application 2 (en groupe) : scénario fictif

En lien avec les jeux olympiques de 2024 à Paris, la ville de Grenoble se pose la question de créer un musée des jeux olympiques à Grenoble.

L'objectif de cette mise en application est de créer des **visualisations de données pour présenter les arguments en faveur et en défaveur de la création d'un nouveau musée à Grenoble**.

Axes d'analyse :

- Gratuité

- Nombre de visiteurs

- Hausse ou baisse sur plusieurs années 

- ...

Deux groupes présenteront (5 minutes de présentation) les visualisations en faveur de la création du musée et les deux autres en défaveur de la création du musée.

Lien vers le jeu de données : https://www.data.gouv.fr/fr/datasets/frequentation-des-musees-de-france/
